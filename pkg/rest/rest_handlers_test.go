package rest_test

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"

	"github.com/go-log/log"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/skeleton/pkg/models"
	"gitlab.com/telo_tade/skeleton/pkg/rest"
	"gitlab.com/telo_tade/skeleton/pkg/service"
)

var testUser = models.User{
	ID:         23,
	DeletionAt: time.Date(2020, 10, 9, 8, 7, 6, 5, time.UTC),
	Email:      "john.doe@gmail.com",
	Name:       "John Doe",
}

type dbClientMock struct {
	getUser, getLatestUser int
}

func (d *dbClientMock) GetUser(ctx context.Context, id uint) (models.User, error) {
	d.getUser++

	return testUser, nil
}

func (d *dbClientMock) GetAllUsers(ctx context.Context) ([]models.User, error) {
	return []models.User{}, nil
}

func (d *dbClientMock) CreateUser(ctx context.Context, p models.User) error {
	return nil
}

func (d *dbClientMock) GetLatestUser(ctx context.Context) (models.User, error) {
	d.getLatestUser++

	return models.User{}, nil
}

func (d *dbClientMock) UpdateLastNotified(ctx context.Context, userID int, deletionAt time.Time) error {
	return nil
}

func TestHandler_GetUser(t *testing.T) {
	req, err := http.NewRequestWithContext(context.Background(), "GET", "localhost:8080/user", nil)
	assert.Nil(t, err, "Error creating request: %v.", err)

	req = mux.SetURLVars(req, map[string]string{
		"ID": "22",
	})

	type fields struct {
		Logger   log.Logger
		DBClient *dbClientMock
	}

	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "ReadAll returns mocked object",
			args: args{
				w: httptest.NewRecorder(),
				r: req,
			},
			fields: fields{
				Logger:   t,
				DBClient: &dbClientMock{},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := rest.Handler{
				Logger:  tt.fields.Logger,
				Service: service.Service{Logger: tt.fields.Logger, DBClient: tt.fields.DBClient},
			}

			h.GetUser(tt.args.w, tt.args.r)

			result := tt.args.w.(*httptest.ResponseRecorder).Result()
			assert.NotNil(t, result, "Response should not be nil.")
			assert.Equal(t, http.StatusOK, result.StatusCode, "Status code should be http.StatusOK.")

			defer result.Body.Close()

			body, err := ioutil.ReadAll(result.Body)
			assert.Nil(t, err, "readAll should produce no error: %v", err)
			assert.Equal(t, 1, tt.fields.DBClient.getUser, "GetUser BD call should be called once.")

			var actual models.User
			err = json.Unmarshal(body, &actual)
			assert.Nil(t, err, "Error should be nil.")
			assert.True(t, reflect.DeepEqual(testUser, actual), "Wrong result value.")
		})
	}
}
