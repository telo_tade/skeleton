/*
Package configsreader offers functionality to read config data from a yaml file. Its main methods is:
	ReadConfigs(filename string) (*Configs, error)
This function will open the yaml file, read its contents and returns a Cofnig struct with the values it has read.
*/
package configsreader

import (
	"flag"
	"fmt"
	"io/ioutil"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

// Configs models the configuration values needed by the "Usage Collector" module.
type Configs struct {
	DBHost   string `json:"dbhost" yaml:"dbhost"`
	DBPort   uint   `json:"dbport" yaml:"dbport"`
	Username string `json:"username" yaml:"username"`
	Password string `json:"password" yaml:"password"`
	Database string `json:"database" yaml:"database"`
	Host     string `json:"host" yaml:"host"`
	Port     uint   `json:"port" yaml:"port"`
}

func (c Configs) String() string {
	return fmt.Sprintf("Configs[DBHost %v, DBPort %v, Username %v, "+
		"Database %v, Host %v, Port %v]",
		c.DBHost, c.DBPort, c.Username, c.Database, c.Host, c.Port)
}

// ReadConfigs reads configs form a yaml file, returns a Cofnig struct with the values it has read.
func ReadConfigs() (*Configs, error) {
	filename := flag.String("config", "config/config.yaml", "Absolute or relative path to the config file")
	flag.Parse()

	yamlFile, err := ioutil.ReadFile(*filename)
	if err != nil {
		return nil, errors.Wrap(err, "Reading the config file at "+*filename+" failed")
	}

	result := Configs{}

	err = yaml.Unmarshal(yamlFile, &result)
	if err != nil {
		return nil, errors.Wrap(err, "Unmarshalling the content of the config file failed")
	}

	return &result, nil
}
