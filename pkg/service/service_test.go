package service_test

import (
	"context"
	"reflect"
	"testing"
	"time"

	"github.com/go-log/log"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/skeleton/pkg/models"
	"gitlab.com/telo_tade/skeleton/pkg/service"
)

func TestService_GetLatestUser(t *testing.T) {
	type fields struct {
		Logger   log.Logger
		DBClient *dbClientMock
	}

	type args struct {
		ctx context.Context
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.User
	}{
		{
			name: "GetLatestUser called once",
			args: args{
				ctx: context.Background(),
			},
			fields: fields{
				Logger:   t,
				DBClient: &dbClientMock{},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.Service{
				Logger:   tt.fields.Logger,
				DBClient: tt.fields.DBClient,
			}

			got, err := s.GetLatestUser(tt.args.ctx)

			assert.Nil(t, err, "Error should be nil.")
			assert.Equal(t, 1, tt.fields.DBClient.getLatestUser, "GetLatestUser should be called only once.")
			assert.True(t, reflect.DeepEqual(tt.want, *got), "Wrong returned user.")
		})
	}
}

func TestService_GetUser(t *testing.T) {
	type fields struct {
		Logger   log.Logger
		DBClient *dbClientMock
	}

	type args struct {
		ctx    context.Context
		userID uint
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.User
	}{
		{
			name: "GetUser called once",
			args: args{
				ctx:    context.Background(),
				userID: 12,
			},
			fields: fields{
				Logger:   t,
				DBClient: &dbClientMock{},
			},
			want: models.User{ID: 12, Email: "test@gmail.com", Name: "John Doe"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.Service{
				Logger:   tt.fields.Logger,
				DBClient: tt.fields.DBClient,
			}
			got, err := s.GetUser(tt.args.ctx, tt.args.userID)

			assert.Nil(t, err, "Error should be nil.")
			assert.Equal(t, 1, tt.fields.DBClient.getUser, "GetUser should be called only once.")
			assert.True(t, reflect.DeepEqual(tt.want, *got), "Wrong returned user.")
		})
	}
}

type dbClientMock struct {
	getUser, getLatestUser int
}

func (d *dbClientMock) GetUser(ctx context.Context, id uint) (models.User, error) {
	d.getUser++

	return models.User{ID: 12, Email: "test@gmail.com", Name: "John Doe"}, nil
}

func (d *dbClientMock) GetAllUsers(ctx context.Context) ([]models.User, error) {
	return []models.User{}, nil
}

func (d *dbClientMock) CreateUser(ctx context.Context, p models.User) error {
	return nil
}

func (d *dbClientMock) GetLatestUser(ctx context.Context) (models.User, error) {
	d.getLatestUser++

	return models.User{}, nil
}

func (d *dbClientMock) UpdateLastNotified(ctx context.Context, userID int, deletionAt time.Time) error {
	return nil
}
