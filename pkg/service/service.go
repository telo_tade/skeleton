package service

import (
	"context"
	"time"

	"github.com/go-log/log"
	"gitlab.com/telo_tade/skeleton/pkg/models"
)

// Service hold the business logic.
type Service struct {
	Logger   log.Logger
	DBClient dbClient
}

// dbClient models interaction with DB.
type dbClient interface {
	GetUser(ctx context.Context, id uint) (models.User, error)
	GetAllUsers(ctx context.Context) ([]models.User, error)
	CreateUser(ctx context.Context, p models.User) error
	GetLatestUser(ctx context.Context) (models.User, error)
	UpdateLastNotified(ctx context.Context, userID int, deletionAt time.Time) error
}

// CreateUser creates a new user.
func (s *Service) CreateUser(ctx context.Context, u models.User) error {
	err := s.DBClient.CreateUser(ctx, u)
	if err != nil {
		s.Logger.Logf("Error creating user: %v.", err)

		return err
	}

	return nil
}

// GetUser returns the user having a given ID.
func (s *Service) GetUser(ctx context.Context, userID uint) (*models.User, error) {
	user, err := s.DBClient.GetUser(ctx, userID)
	if err != nil {
		s.Logger.Logf("Error getting user from db, msg: %v.", err)

		return nil, err
	}

	return &user, nil
}

// GetAllUsers returns all users.
func (s *Service) GetAllUsers(ctx context.Context) ([]models.User, error) {
	users, err := s.DBClient.GetAllUsers(ctx)
	if err != nil {
		s.Logger.Logf("Error getting all users %v.", err)

		return nil, err
	}

	return users, nil
}

// GetLatestUser returns the user having a given ID.
func (s *Service) GetLatestUser(ctx context.Context) (*models.User, error) {
	user, err := s.DBClient.GetLatestUser(ctx)
	if err != nil {
		s.Logger.Logf("Error getting user from db, msg: %v.", err)

		return nil, err
	}

	return &user, nil
}
