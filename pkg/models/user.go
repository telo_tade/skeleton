package models

import (
	"fmt"
	"time"
)

// User models a user, as needed by the DB layer.
type User struct {
	ID         int       `json:"id" gorm:"primary_key;autoIncrement"`
	Name       string    `json:"name"`
	DeletionAt time.Time `json:"deletion_at"`
	InsertedAt time.Time `json:"inserted_at"`
	Email      string    `json:"email"`
}

func (u User) String() string {
	return fmt.Sprintf("User[ID %v, Name %v, DeletionAt %v, Email %v]", u.ID, u.Name, u.DeletionAt, u.Email)
}
