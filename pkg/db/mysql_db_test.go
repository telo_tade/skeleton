package db_test

import (
	"context"
	"reflect"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/skeleton/pkg/db"
	"gitlab.com/telo_tade/skeleton/pkg/models"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func createMySQLDBClientMock() (gdb *gorm.DB, mock sqlmock.Sqlmock, err error) {
	handler, mock, err := sqlmock.New()
	if err != nil {
		return nil, nil, err
	}

	gdb, err = gorm.Open(mysql.New(mysql.Config{
		DriverName:                "mocked_mysql",
		DSN:                       "",
		SkipInitializeWithVersion: true,
		Conn:                      handler,
	}), &gorm.Config{})
	if err != nil {
		return nil, nil, err
	}

	return gdb, mock, nil
}

func TestNewMySQLClientNoConnection(t *testing.T) {
	client, err := db.NewClient("a", 1, "b", "c", "d")
	assert.Nil(t, client, "Client should  be nil.")
	assert.NotNil(t, err, "Error should not be nil.")
}

func TestMySQLCreateUser(t *testing.T) {
	gdb, mock, err := createMySQLDBClientMock()
	assert.Nil(t, err, "Error should be nil.")

	mockTime := time.Date(2000, 8, 25, 3, 0, 0, 0, time.Local)
	p := models.User{
		ID:         95,
		Name:       "mcdr",
		DeletionAt: mockTime,
		Email:      "foo@example.net",
	}

	mock.ExpectExec(regexp.QuoteMeta(
		"INSERT INTO `users` (`name`,`deletion_at`,`inserted_at`,`email`,`id`) VALUES (?,?,?,?,?)")).
		WithArgs(p.Name, p.DeletionAt, sqlmock.AnyArg(), p.Email, p.ID).
		WillReturnResult(sqlmock.NewResult(95, 1))

	client := db.PostgresClient{DB: gdb}
	err = client.CreateUser(context.Background(), p)
	assert.Nil(t, err, "Error should be nil.")

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "Error should be nil.")
}

func TestMySQLGetUser(t *testing.T) {
	gdb, mock, err := createMySQLDBClientMock()
	assert.Nil(t, err, "Error should be nil")

	now := time.Date(2020, 8, 18, 22, 12, 1, 11, time.UTC)
	expected := models.User{
		ID:         12,
		Name:       "Tobias",
		DeletionAt: now,
		Email:      "tobias@metronom",
	}
	rows := mock.NewRows(
		[]string{"id", "name", "deletion_at", "email"},
	).AddRow(expected.ID, expected.Name, now, expected.Email)

	mock.ExpectQuery(regexp.QuoteMeta(
		"SELECT * FROM `users` WHERE id = ?")).
		WithArgs(12).
		WillReturnRows(rows)

	client := db.PostgresClient{DB: gdb}
	p, err := client.GetUser(context.Background(), 12)
	assert.Nil(t, err, "Error should be nil.")
	assert.True(t, reflect.DeepEqual(expected, p), "Wrong user value.")

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "Error should be nil.")
}

func TestMySQLUpdateUser(t *testing.T) {
	gdb, mock, err := createMySQLDBClientMock()
	assert.Nil(t, err, "Error should be nil.")

	mockTime := time.Date(2020, 8, 18, 22, 12, 1, 11, time.Local)

	p := models.User{
		ID:         95,
		Name:       "mcdr",
		DeletionAt: mockTime,
		Email:      "tobias@metronom",
	}

	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta(
		"UPDATE `users` SET `id`=?,`name`=?,`deletion_at`=?,`email`=? WHERE `id` = ?")).
		WithArgs(p.ID, p.Name, p.DeletionAt, p.Email, p.ID).
		WillReturnResult(sqlmock.NewResult(95, 1))
	mock.ExpectCommit()

	client := db.PostgresClient{DB: gdb}

	err = client.UpdateUser(context.Background(), p)
	assert.Nil(t, err, "Error should be nil.")

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "Error should be nil.")
}

func TestMySQLDeleteUser(t *testing.T) {
	gdb, mock, err := createMySQLDBClientMock()
	assert.Nil(t, err, "Error should be nil.")

	mockTime := time.Date(2020, 7, 19, 22, 12, 1, 11, time.Local)

	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta(
		"DELETE FROM `users` WHERE `users`.`id` = ?")).
		WithArgs(95).
		WillReturnResult(sqlmock.NewResult(95, 1))
	mock.ExpectCommit()

	client := db.PostgresClient{DB: gdb}
	p := models.User{
		ID:         95,
		Name:       "mcdr",
		DeletionAt: mockTime,
		Email:      "tobias@metronom",
	}
	err = client.DeleteUser(context.Background(), p)
	assert.Nil(t, err, "Error should be nil.")

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "Error should be nil.")
}

func TestMySQLGetAllUsers(t *testing.T) {
	gdb, mock, err := createMySQLDBClientMock()
	assert.Nil(t, err, "error should be nil.")

	now := time.Date(2020, 8, 18, 22, 12, 1, 11, time.UTC)
	expected := []models.User{{
		ID:         95,
		Name:       "Tobias",
		DeletionAt: now,
		Email:      "tobias@metronom",
	}, {
		ID:         95,
		Name:       "Markus",
		DeletionAt: now,
		Email:      "Markus@metronom",
	}}
	rows := mock.NewRows(
		[]string{"id", "name", "deletion_at", "email"},
	).
		AddRow(expected[0].ID, expected[0].Name, now, expected[0].Email).
		AddRow(expected[1].ID, expected[1].Name, now, expected[1].Email)

	mock.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `users`")).
		WithArgs().
		WillReturnRows(rows)

	client := db.PostgresClient{DB: gdb}
	res, err := client.GetAllUsers(context.Background())
	assert.Nil(t, err, "Error should be nil.")

	assert.True(t, reflect.DeepEqual(expected, res), "Wrong user value.")

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "Error should be nil.")
}

func TestMySQLGetLatestUser(t *testing.T) {
	gdb, mock, err := createMySQLDBClientMock()
	assert.Nil(t, err, "Error should be nil")

	now := time.Date(2020, 8, 18, 22, 12, 1, 11, time.UTC)
	expected := models.User{
		ID:         12,
		Name:       "Tobias",
		DeletionAt: now,
		Email:      "tobias@metronom",
	}
	rows := mock.NewRows(
		[]string{"id", "name", "deletion_at", "email"},
	).AddRow(expected.ID, expected.Name, now, expected.Email)

	mock.ExpectQuery(regexp.QuoteMeta(
		"SELECT * FROM `users` ORDER BY inserted_at desc,`users`.`id` LIMIT 1")).
		WillReturnRows(rows)

	client := db.PostgresClient{DB: gdb}
	p, err := client.GetLatestUser(context.Background())
	assert.Nil(t, err, "Error should be nil.")
	assert.True(t, reflect.DeepEqual(expected, p), "Wrong user value.")

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "Error should be nil.")
}
