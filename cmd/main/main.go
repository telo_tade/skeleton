package main

import (
	"context"
	"flag"
	"os"

	"github.com/google/subcommands"
	"gitlab.com/telo_tade/skeleton/pkg/commands"
	configsreader "gitlab.com/telo_tade/skeleton/pkg/configs"
	"gitlab.com/telo_tade/skeleton/pkg/db"
	"gitlab.com/telo_tade/skeleton/pkg/logger"
	"gitlab.com/telo_tade/skeleton/pkg/rest"
	"gitlab.com/telo_tade/skeleton/pkg/service"
)

// GitCommit is used to inject the commit number.
var GitCommit = "local_build"

// Version is used to inject the version number.
var Version = "unknown"

func main() {
	configs, err := configsreader.ReadConfigs()
	if err != nil {
		panic(err.Error())
	}

	l := logger.InitLogger()
	l.Logf("µS - commit: %v, version: %v", GitCommit, Version)

	db, err := db.NewClient(configs.DBHost, configs.DBPort, configs.Username, configs.Password, configs.Database)
	if err != nil {
		panic(err.Error())
	}

	service := service.Service{Logger: l, DBClient: db}
	handler := rest.Handler{Logger: l, Service: service}
	server := rest.NewServer(configs.Host, configs.Port, l, handler)

	subcommands.Register(subcommands.HelpCommand(), "")
	subcommands.Register(subcommands.FlagsCommand(), "")
	subcommands.Register(subcommands.CommandsCommand(), "")

	subcommands.Register(&commands.APICmd{
		Server: server,
	}, "")

	flag.Parse()

	ctx := context.Background()
	os.Exit(int(subcommands.Execute(ctx)))
}
