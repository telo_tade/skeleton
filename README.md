# REST API Micro service skeleton

Offers a REST API to CRUD user object, on top of a DB table.

## How to run

Api mode:

go run api/main/main.go -config="path to config file" api

## Functionality

This µService exposes a REST API to manage user objects.
